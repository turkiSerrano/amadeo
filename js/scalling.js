$(function() {

    $("#Sound").on("mouseover", scalling);
    $("#Picture").on("mouseover", scalling);
    $("#twin-conteiner section").on("mouseout", backScale);

    function scalling(e) {
        if ($(this).hasClass() === false) {
            $(this).addClass("scaleOn");
            $(this).siblings().addClass("scaleOff");
        }
        if ($("#twin-conteiner section").hasClass("scaleNone")) {
            $("#twin-conteiner section").removeClass("scaleNone");
            $(this).removeClass("scaleOff scaleOn").addClass("scaleOn");
            $(this).siblings().removeClass("scaleOff scaleOn").addClass("scaleOff");
        }
    }

    function backScale(e) {
        $("#twin-conteiner section").removeClass("scaleOn scaleOff").addClass("scaleNone");
    }
})